<div class="container">
    <div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
        <div class="text-center">

            <h3 class="mb-5">Controle Financeiro</h3>

            <form method="POST">
        
                <div class="form-outline mb-4">
                    <input type="email" id="email" name="email" class="form-control" required />
                    <label class="form-label" for="email">E-mail</label>
                </div>

                <div class="form-outline mb-4">
                    <input type="password" id="senha" name="senha" class="form-control" required />
                    <label class="form-label" for="senha">Senha</label>
                </div>

                <div class="row mb-4">
                    <div class="col d-flex justify-content-center">

                    </div>

                </div>

                <button type="submit" class="btn btn-primary btn-block mb-4">Entrar</button>
                
                <p class="red-text"> <?= $error ? 'Dados de acesso incorretos.' : '' ?> </p>
            </form>

        </div>
    </div>
</div>